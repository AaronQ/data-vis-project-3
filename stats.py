# Explores the data to determine statistics.
import copy

import numpy as np
import pandas as pd
import statistics

def read_file(file_name):
  """
  reads in the file and puts the data into lists
  :param file_name: name of the input file
  :return: total_meas: list of lists containing list of  measurement values
  """
  with open(file_name, 'r') as f:
    variables = f.readline().rstrip().split(',')
    temp_variables = [variables[0]]
    variables = temp_variables + variables[4::]
    total_meas = []
    laken_meas = []
    lakenw_meas = []
    lakese_meas = []
    lakesw_meas = []
    for line in f:
      line_txt = line.rstrip().split(',')
      total_meas.append(line_txt)
      if line_txt[1] == 'LakeN':
          laken_meas.append(line_txt)
      elif line_txt[1] == 'LakeNW':
          lakenw_meas.append(line_txt)
      elif line_txt[1] == 'LakeSE':
          lakese_meas.append(line_txt)
      elif line_txt[1] == 'LakeSW':
          lakesw_meas.append(line_txt)
  return total_meas, laken_meas, lakenw_meas, lakese_meas, lakesw_meas, variables


def min_values(vector):
  """
  determines the minimum value for a list
  :param vector: list of values
  :return: min_value: minimum value of vector
  """
  min_value = vector[0]
  for i in range(0, len(vector)):
    min_value = vector[i] if vector[i] < min_value else min_value
  return min_value


def max_values(vector):
  """
  determines the minimum value for a list
  :param vector: list of values
  :return: min_value: minimum value of vector
  """
  max_value = vector[0]
  for i in range(0, len(vector)):
    max_value = vector[i] if vector[i] > max_value else max_value
  return max_value


def mean_calc(vector):
  """
  determines the mean for list of values
  :param vector: list of the values
  :result mean_value: mean of vector values
  """
  mean_value = float(vector[0])
  for i in range(1, len(vector)):
    mean_value = mean_value + float(vector[i])
  mean_value = round(mean_value/len(vector), 4)
  return mean_value


def median_calc(vector):
  """
  determines the median for list of values
  :param vector: list of the values
  :result median_value: median of vector values
  """
  median_value = float(vector[0])
  for i in range(1, len(vector)):
    mean_value = mean_value + float(vector[i])
  mean_value = round(mean_value/len(vector), 4)
  return median_value

def trim_mean_calc(vector, p):
  """
  calculates the mean for vector values after the first and last p number of elements are removed
  from the sorted list
  :param vector: list of lists containing values
  :param p: number of elements to trim
  :return: trim_value: list of lists containing the trim mean values
  """
  values = copy.deepcopy(vector)
  values.sort()
  del values[-p:]
  del values[:p]
  values = np.array(values)
  trim_value = float(values[0])
  for i in range(1, len(values) - 1):
    trim_value = trim_value + float(values[i])
  trim_value = round(trim_value/len(values), 4)
  return trim_value


def stan_dev(vector, mean_value):
  """
  Calculates the standard deviation of the vector data
  :param data_matrix: list of lists containing values
  :param mean_value: mean value of the vector
  :return: stan_dev: list containing the standard deviation values
  """
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    values = values ** 2
    stan_dev = stan_dev + values
  stan_dev = (stan_dev / (len(vector) - 1))
  stan_dev = stan_dev ** (1/2)
  return round(stan_dev, 4)


def skew_calc(vector, mean_value):
  """
  Calculates the skewness of the vector data
  :param vector: list containing values
  :param mean_value: mean value of vector
  :return: skew_val: skewness values
  """
  skew_val = 0
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    stan_val = values ** 2
    values = values ** 3
    skew_val = skew_val + values
    stan_dev = stan_dev + stan_val
  skew_val = (skew_val / len(vector))
  stan_dev = (stan_dev / (len(vector)))
  stan_dev = stan_dev ** (3/2)
  if stan_dev == 0:
      return 0
  skew_val = skew_val/stan_dev
  return round(skew_val, 4)

def average_direction(speed, direction):
    """Calculate the average wind direction using wind speed and direction"""
    direction_ew = [speed[i]*np.sin(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ew_average = sum(direction_ew)/len(direction_ew)
    direction_ns = [speed[i]*np.cos(direction[i]*np.pi/180) for i in range(0, len(direction), 1)]
    direction_ns_average = sum(direction_ns)/len(direction_ns)
    direction_average = np.arctan2(direction_ew_average,direction_ns_average) * 180/np.pi
    if direction_average < 0:
        direction_average = direction_average + 360
    return round(direction_average, 4)


def kurtosis_calc(vector, mean_value):
  """
  Calculates the skewness of the vector data
  :param vector: list containing values
  :param mean_value: mean value of vector
  :return: kurt_val: kurtosis values
  """
  kurt_val = 0
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    stan_val = values ** 2
    values = values ** 4
    kurt_val = kurt_val + values
    stan_dev = stan_dev + stan_val
  kurt_val = (kurt_val / len(vector))
  stan_dev = (stan_dev / (len(vector)))
  stan_dev = stan_dev ** (4/2)
  if stan_dev == 0:
      return 0
  kurt_val = kurt_val/stan_dev
  return round(kurt_val, 4)

if __name__ == '__main__':
  file_name = 'output.csv'
  total_meas, laken_meas, lakenw_meas, lakese_meas, lakesw_meas, variables = read_file(file_name)
  total_meas = list(map(list, zip(*total_meas)))
  laken_meas = list(map(list, zip(*laken_meas)))
  lakenw_meas = list(map(list, zip(*lakenw_meas)))
  lakese_meas = list(map(list, zip(*lakese_meas)))
  lakesw_meas = list(map(list, zip(*lakesw_meas)))

  # Convert values of measurements to numbers
  total_meas[4] = [float(x) for x in total_meas[4]]
  total_meas[5] = [float(x) for x in total_meas[5]]
  total_meas[6] = [float(x) for x in total_meas[6]]
  total_meas[7] = [float(x) for x in total_meas[7]]
  total_meas[8] = [float(x) for x in total_meas[8]]
  total_meas[10] = [float(x) for x in total_meas[10]]
  total_meas[11] = [float(x) for x in total_meas[11]]
  total_meas[12] = [float(x) for x in total_meas[12]]
  total_meas[13] = [float(x) for x in total_meas[13]]

  laken_meas[4] = [float(x) for x in laken_meas[4]]
  laken_meas[5] = [float(x) for x in laken_meas[5]]
  laken_meas[6] = [float(x) for x in laken_meas[6]]
  laken_meas[7] = [float(x) for x in laken_meas[7]]
  laken_meas[8] = [float(x) for x in laken_meas[8]]
  laken_meas[10] = [float(x) for x in laken_meas[10]]
  laken_meas[11] = [float(x) for x in laken_meas[11]]
  laken_meas[12] = [float(x) for x in laken_meas[12]]
  laken_meas[13] = [float(x) for x in laken_meas[13]]

  lakenw_meas[4] = [float(x) for x in lakenw_meas[4]]
  lakenw_meas[5] = [float(x) for x in lakenw_meas[5]]
  lakenw_meas[6] = [float(x) for x in lakenw_meas[6]]
  lakenw_meas[7] = [float(x) for x in lakenw_meas[7]]
  lakenw_meas[8] = [float(x) for x in lakenw_meas[8]]
  lakenw_meas[10] = [float(x) for x in lakenw_meas[10]]
  lakenw_meas[11] = [float(x) for x in lakenw_meas[11]]
  lakenw_meas[12] = [float(x) for x in lakenw_meas[12]]
  lakenw_meas[13] = [float(x) for x in lakenw_meas[13]]

  lakese_meas[4] = [float(x) for x in lakese_meas[4]]
  lakese_meas[5] = [float(x) for x in lakese_meas[5]]
  lakese_meas[6] = [float(x) for x in lakese_meas[6]]
  lakese_meas[7] = [float(x) for x in lakese_meas[7]]
  lakese_meas[8] = [float(x) for x in lakese_meas[8]]
  lakese_meas[10] = [float(x) for x in lakese_meas[10]]
  lakese_meas[11] = [float(x) for x in lakese_meas[11]]
  lakese_meas[12] = [float(x) for x in lakese_meas[12]]
  lakese_meas[13] = [float(x) for x in lakese_meas[13]]

  lakesw_meas[4] = [float(x) for x in lakesw_meas[4]]
  lakesw_meas[5] = [float(x) for x in lakesw_meas[5]]
  lakesw_meas[6] = [float(x) for x in lakesw_meas[6]]
  lakesw_meas[7] = [float(x) for x in lakesw_meas[7]]
  lakesw_meas[8] = [float(x) for x in lakesw_meas[8]]
  lakesw_meas[10] = [float(x) for x in lakesw_meas[10]]
  lakesw_meas[11] = [float(x) for x in lakesw_meas[11]]
  lakesw_meas[12] = [float(x) for x in lakesw_meas[12]]
  lakesw_meas[13] = [float(x) for x in lakesw_meas[13]]

  # remove measurements for when no wind is present
  for i in range(len(total_meas[6]) - 1, -1, -1):
      if total_meas[6][i] == 0:
        del total_meas[0][i]
        del total_meas[1][i]
        del total_meas[2][i]
        del total_meas[3][i]
        del total_meas[4][i]
        del total_meas[5][i]
        del total_meas[6][i]
        del total_meas[7][i]
        del total_meas[8][i]
        del total_meas[9][i]
        del total_meas[10][i]
        del total_meas[11][i]
        del total_meas[12][i]
        del total_meas[13][i]

  for i in range(len(laken_meas[6]) - 1, -1, -1):
      if laken_meas[6][i] == 0:
        del laken_meas[0][i]
        del laken_meas[1][i]
        del laken_meas[2][i]
        del laken_meas[3][i]
        del laken_meas[4][i]
        del laken_meas[5][i]
        del laken_meas[6][i]
        del laken_meas[7][i]
        del laken_meas[8][i]
        del laken_meas[9][i]
        del laken_meas[10][i]
        del laken_meas[11][i]
        del laken_meas[12][i]
        del laken_meas[13][i]

  for i in range(len(lakenw_meas[6]) - 1, -1, -1):
      if lakenw_meas[6][i] == 0:
        del lakenw_meas[0][i]
        del lakenw_meas[1][i]
        del lakenw_meas[2][i]
        del lakenw_meas[3][i]
        del lakenw_meas[4][i]
        del lakenw_meas[5][i]
        del lakenw_meas[6][i]
        del lakenw_meas[7][i]
        del lakenw_meas[8][i]
        del lakenw_meas[9][i]
        del lakenw_meas[10][i]
        del lakenw_meas[11][i]
        del lakenw_meas[12][i]
        del lakenw_meas[13][i]

  for i in range(len(lakese_meas[6]) - 1, -1, -1):
      if lakese_meas[6][i] == 0:
        del lakese_meas[0][i]
        del lakese_meas[1][i]
        del lakese_meas[2][i]
        del lakese_meas[3][i]
        del lakese_meas[4][i]
        del lakese_meas[5][i]
        del lakese_meas[6][i]
        del lakese_meas[7][i]
        del lakese_meas[8][i]
        del lakese_meas[9][i]
        del lakese_meas[10][i]
        del lakese_meas[11][i]
        del lakese_meas[12][i]
        del lakese_meas[13][i]

  for i in range(len(lakesw_meas[6]) - 1, -1, -1):
      if lakesw_meas[6][i] == 0:
        del lakesw_meas[0][i]
        del lakesw_meas[1][i]
        del lakesw_meas[2][i]
        del lakesw_meas[3][i]
        del lakesw_meas[4][i]
        del lakesw_meas[5][i]
        del lakesw_meas[6][i]
        del lakesw_meas[7][i]
        del lakesw_meas[8][i]
        del lakesw_meas[9][i]
        del lakesw_meas[10][i]
        del lakesw_meas[11][i]
        del lakesw_meas[12][i]
        del lakesw_meas[13][i]

  # calculate minimums
  total_mins = ['-', '-', '-', '-', min_values(total_meas[4]), min_values(total_meas[5]), min_values(total_meas[6]),
                min_values(total_meas[7]), min_values(total_meas[8]), '-', min_values(total_meas[10]),
                min_values(total_meas[11]), min_values(total_meas[12]), min_values(total_meas[13])]
  laken_mins = ['-', '-', '-', '-', min_values(laken_meas[4]), min_values(laken_meas[5]), min_values(laken_meas[6]),
                min_values(laken_meas[7]), min_values(laken_meas[8]), '-', min_values(laken_meas[10]),
                min_values(laken_meas[11]), min_values(laken_meas[12]), min_values(laken_meas[13])]
  lakenw_mins = ['-', '-', '-', '-', min_values(lakenw_meas[4]), min_values(lakenw_meas[5]), min_values(lakenw_meas[6]),
                min_values(lakenw_meas[7]), min_values(lakenw_meas[8]), '-', min_values(lakenw_meas[10]),
                min_values(lakenw_meas[11]), min_values(lakenw_meas[12]), min_values(lakenw_meas[13])]
  lakese_mins = ['-', '-', '-', '-', min_values(lakese_meas[4]), min_values(lakese_meas[5]), min_values(lakese_meas[6]),
                min_values(lakese_meas[7]), min_values(lakese_meas[8]), '-', min_values(lakese_meas[10]),
                min_values(lakese_meas[11]), min_values(lakese_meas[12]), min_values(lakese_meas[13])]
  lakesw_mins = ['-', '-', '-', '-', min_values(lakesw_meas[4]), min_values(lakesw_meas[5]), min_values(lakesw_meas[6]),
                min_values(lakesw_meas[7]), min_values(lakesw_meas[8]), '-', min_values(lakesw_meas[10]),
                min_values(lakesw_meas[11]), min_values(lakesw_meas[12]), min_values(lakesw_meas[13])]
  # calculate maximums
  total_maxs = ['-', '-', '-', '-', max_values(total_meas[4]), max_values(total_meas[5]), max_values(total_meas[6]),
                max_values(total_meas[7]), max_values(total_meas[8]), '-', max_values(total_meas[10]),
                max_values(total_meas[11]), max_values(total_meas[12]), max_values(total_meas[13])]
  laken_maxs = ['-', '-', '-', '-', max_values(laken_meas[4]), max_values(laken_meas[5]), max_values(laken_meas[6]),
                max_values(laken_meas[7]), max_values(laken_meas[8]), '-', max_values(laken_meas[10]),
                max_values(laken_meas[11]), max_values(laken_meas[12]), max_values(laken_meas[13])]
  lakenw_maxs = ['-', '-', '-', '-', max_values(lakenw_meas[4]), max_values(lakenw_meas[5]), max_values(lakenw_meas[6]),
                max_values(lakenw_meas[7]), max_values(lakenw_meas[8]), '-', max_values(lakenw_meas[10]),
                max_values(lakenw_meas[11]), max_values(lakenw_meas[12]), max_values(lakenw_meas[13])]
  lakese_maxs = ['-', '-', '-', '-', max_values(lakese_meas[4]), max_values(lakese_meas[5]), max_values(lakese_meas[6]),
                max_values(lakese_meas[7]), max_values(lakese_meas[8]), '-', max_values(lakese_meas[10]),
                max_values(lakese_meas[11]), max_values(lakese_meas[12]), max_values(lakese_meas[13])]
  lakesw_maxs = ['-', '-', '-', '-', max_values(lakesw_meas[4]), max_values(lakesw_meas[5]), max_values(lakesw_meas[6]),
                max_values(lakesw_meas[7]), max_values(lakesw_meas[8]), '-', max_values(lakesw_meas[10]),
                max_values(lakesw_meas[11]), max_values(lakesw_meas[12]), max_values(lakesw_meas[13])]
 # calculate means
  total_means = ['-', '-', '-', '-', mean_calc(total_meas[4]), mean_calc(total_meas[5]), mean_calc(total_meas[6]),
                 mean_calc(total_meas[7]), average_direction(total_meas[6], total_meas[8]), '-', mean_calc(total_meas[10]),
                 mean_calc(total_meas[11]), mean_calc(total_meas[12]), mean_calc(total_meas[13])]
  laken_means = ['-', '-', '-', '-', mean_calc(laken_meas[4]), mean_calc(laken_meas[5]), mean_calc(laken_meas[6]),
                 mean_calc(laken_meas[7]), average_direction(laken_meas[6], laken_meas[8]), '-', mean_calc(laken_meas[10]),
                 mean_calc(laken_meas[11]), mean_calc(laken_meas[12]), mean_calc(laken_meas[13])]
  lakenw_means = ['-', '-', '-', '-', mean_calc(lakenw_meas[4]), mean_calc(lakenw_meas[5]), mean_calc(lakenw_meas[6]),
                 mean_calc(lakenw_meas[7]), average_direction(lakenw_meas[6], lakenw_meas[8]), '-', mean_calc(lakenw_meas[10]),
                 mean_calc(lakenw_meas[11]), mean_calc(lakenw_meas[12]), mean_calc(lakenw_meas[13])]
  lakese_means = ['-', '-', '-', '-', mean_calc(lakese_meas[4]), mean_calc(lakese_meas[5]), mean_calc(lakese_meas[6]),
                 mean_calc(lakese_meas[7]), average_direction(lakese_meas[6], lakese_meas[8]), '-', mean_calc(lakese_meas[10]),
                 mean_calc(lakese_meas[11]), mean_calc(lakese_meas[12]), mean_calc(lakese_meas[13])]
  lakesw_means = ['-', '-', '-', '-', mean_calc(lakesw_meas[4]), mean_calc(lakesw_meas[5]), mean_calc(lakesw_meas[6]),
                 mean_calc(lakesw_meas[7]), average_direction(lakesw_meas[6], lakesw_meas[8]), '-', mean_calc(lakesw_meas[10]),
                 mean_calc(lakesw_meas[11]), mean_calc(lakesw_meas[12]), mean_calc(lakesw_meas[13])]
  # calculate trimmed means
  total_trim_means = ['-', '-', '-', '-', trim_mean_calc(total_meas[4], 2), trim_mean_calc(total_meas[5], 2),
                      trim_mean_calc(total_meas[6], 2), trim_mean_calc(total_meas[7], 2),
                      '-', '-', trim_mean_calc(total_meas[10], 2),
                      trim_mean_calc(total_meas[11], 2), trim_mean_calc(total_meas[12], 2),
                      trim_mean_calc(total_meas[13], 2)]

  total_trim_means = ['-', '-', '-', '-', trim_mean_calc(total_meas[4], 2), trim_mean_calc(total_meas[5], 2),
                      trim_mean_calc(total_meas[6], 2), trim_mean_calc(total_meas[7], 2),
                      '-', '-', trim_mean_calc(total_meas[10], 2),
                      trim_mean_calc(total_meas[11], 2), trim_mean_calc(total_meas[12], 2),
                      trim_mean_calc(total_meas[13], 2)]
  laken_trim_means = ['-', '-', '-', '-', trim_mean_calc(laken_meas[4], 2), trim_mean_calc(laken_meas[5], 2),
                      trim_mean_calc(laken_meas[6], 2), trim_mean_calc(laken_meas[7], 2),
                      '-', '-', trim_mean_calc(laken_meas[10], 2),
                      trim_mean_calc(laken_meas[11], 2), trim_mean_calc(laken_meas[12], 2),
                      trim_mean_calc(laken_meas[13], 2)]
  lakenw_trim_means = ['-', '-', '-', '-', trim_mean_calc(lakenw_meas[4], 2), trim_mean_calc(lakenw_meas[5], 2),
                      trim_mean_calc(lakenw_meas[6], 2), trim_mean_calc(lakenw_meas[7], 2),
                      '-', '-', trim_mean_calc(lakenw_meas[10], 2),
                      trim_mean_calc(lakenw_meas[11], 2), trim_mean_calc(lakenw_meas[12], 2),
                      trim_mean_calc(lakenw_meas[13], 2)]
  lakese_trim_means = ['-', '-', '-', '-', trim_mean_calc(lakese_meas[4], 2), trim_mean_calc(lakese_meas[5], 2),
                      trim_mean_calc(lakese_meas[6], 2), trim_mean_calc(lakese_meas[7], 2),
                      '-', '-', trim_mean_calc(lakese_meas[10], 2),
                      trim_mean_calc(lakese_meas[11], 2), trim_mean_calc(lakese_meas[12], 2),
                      trim_mean_calc(lakese_meas[13], 2)]
  lakesw_trim_means = ['-', '-', '-', '-', trim_mean_calc(lakesw_meas[4], 2), trim_mean_calc(lakesw_meas[5], 2),
                     trim_mean_calc(lakesw_meas[6], 2), trim_mean_calc(lakesw_meas[7], 2),
                     '-', '-', trim_mean_calc(lakesw_meas[10], 2),
                     trim_mean_calc(lakesw_meas[11], 2), trim_mean_calc(lakesw_meas[12], 2),
                     trim_mean_calc(lakesw_meas[13], 2)]
  # caclulate the mode
  total_modes = ['-', '-', '-', '-', statistics.mode(total_meas[4]), statistics.mode(total_meas[5]), statistics.mode(total_meas[6]),
                 statistics.mode(total_meas[7]), statistics.mode(total_meas[8]), statistics.mode(total_meas[9]),
                 statistics.mode(total_meas[10]), statistics.mode(total_meas[11]), statistics.mode(total_meas[12]),
                 statistics.mode(total_meas[13])]

  laken_modes = ['-', '-', '-', '-', statistics.mode(laken_meas[4]), statistics.mode(laken_meas[5]),
                 statistics.mode(laken_meas[6]),
                 statistics.mode(laken_meas[7]), statistics.mode(laken_meas[8]), statistics.mode(laken_meas[9]),
                 statistics.mode(laken_meas[10]), statistics.mode(laken_meas[11]), statistics.mode(laken_meas[12]),
                 statistics.mode(laken_meas[13])]
  lakenw_modes = ['-', '-', '-', '-', statistics.mode(lakenw_meas[4]), statistics.mode(lakenw_meas[5]),
                 statistics.mode(lakenw_meas[6]),
                 statistics.mode(lakenw_meas[7]), statistics.mode(lakenw_meas[8]), statistics.mode(lakenw_meas[9]),
                 statistics.mode(lakenw_meas[10]), statistics.mode(lakenw_meas[11]), statistics.mode(lakenw_meas[12]),
                 statistics.mode(lakenw_meas[13])]
  lakese_modes = ['-', '-', '-', '-', statistics.mode(lakese_meas[4]), statistics.mode(lakese_meas[5]),
                 statistics.mode(lakese_meas[6]),
                 statistics.mode(lakese_meas[7]), statistics.mode(lakese_meas[8]), statistics.mode(lakese_meas[9]),
                 statistics.mode(lakese_meas[10]), statistics.mode(lakese_meas[11]), statistics.mode(lakese_meas[12]),
                 statistics.mode(lakese_meas[13])]
  lakesw_modes = ['-', '-', '-', '-', statistics.mode(lakesw_meas[4]), statistics.mode(lakesw_meas[5]),
                 statistics.mode(lakesw_meas[6]),
                 statistics.mode(lakesw_meas[7]), statistics.mode(lakesw_meas[8]), statistics.mode(lakesw_meas[9]),
                 statistics.mode(lakesw_meas[10]), statistics.mode(lakesw_meas[11]), statistics.mode(lakesw_meas[12]),
                 statistics.mode(lakesw_meas[13])]
  # calculate the median
  total_median = ['-', '-', '-', '-', statistics.median(total_meas[4]), statistics.median(total_meas[5]),
                  statistics.median(total_meas[6]), statistics.median(total_meas[7]),
                  statistics.median(total_meas[8]), '-', statistics.median(total_meas[10]),
                  statistics.median(total_meas[11]), statistics.median(total_meas[12]), statistics.median(total_meas[13])]

  laken_median = ['-', '-', '-', '-', statistics.median(laken_meas[4]), statistics.median(laken_meas[5]),
                  statistics.median(laken_meas[6]), statistics.median(laken_meas[7]),
                  statistics.median(laken_meas[8]), '-', statistics.median(laken_meas[10]),
                  statistics.median(laken_meas[11]), statistics.median(laken_meas[12]), statistics.median(laken_meas[13])]
  lakenw_median = ['-', '-', '-', '-', statistics.median(lakenw_meas[4]), statistics.median(lakenw_meas[5]),
                  statistics.median(lakenw_meas[6]), statistics.median(lakenw_meas[7]),
                  statistics.median(lakenw_meas[8]), '-', statistics.median(lakenw_meas[10]),
                  statistics.median(lakenw_meas[11]), statistics.median(lakenw_meas[12]), statistics.median(lakenw_meas[13])]
  lakese_median = ['-', '-', '-', '-', statistics.median(lakese_meas[4]), statistics.median(lakese_meas[5]),
                  statistics.median(lakese_meas[6]), statistics.median(lakese_meas[7]),
                  statistics.median(lakese_meas[8]), '-', statistics.median(lakese_meas[10]),
                  statistics.median(lakese_meas[11]), statistics.median(lakese_meas[12]), statistics.median(lakese_meas[13])]
  lakesw_median = ['-', '-', '-', '-', statistics.median(lakesw_meas[4]), statistics.median(lakesw_meas[5]),
                  statistics.median(lakesw_meas[6]), statistics.median(lakesw_meas[7]),
                  statistics.median(lakesw_meas[8]), '-', statistics.median(lakesw_meas[10]),
                  statistics.median(lakesw_meas[11]), statistics.median(lakesw_meas[12]), statistics.median(lakesw_meas[13])]
  # calculate standard deviations
  total_stan_deviat = ['-', '-', '-', '-', stan_dev(total_meas[4], total_means[4]),
                       stan_dev(total_meas[5], total_means[5]), stan_dev(total_meas[6], total_means[6]),
                       stan_dev(total_meas[7], total_means[7]), stan_dev(total_meas[8], total_means[8]), '-',
                       stan_dev(total_meas[10], total_means[10]), stan_dev(total_meas[11], total_means[11]),
                       stan_dev(total_meas[12], total_means[12]), stan_dev(total_meas[13], total_means[13])]

  laken_stan_deviat = ['-', '-', '-', '-', stan_dev(laken_meas[4], laken_means[4]),
                       stan_dev(laken_meas[5], laken_means[5]), stan_dev(laken_meas[6], laken_means[6]),
                       stan_dev(laken_meas[7], laken_means[7]), stan_dev(laken_meas[8], laken_means[8]), '-',
                       stan_dev(laken_meas[10], laken_means[10]), stan_dev(laken_meas[11], laken_means[11]),
                       stan_dev(laken_meas[12], laken_means[12]), stan_dev(laken_meas[13], laken_means[13])]
  lakenw_stan_deviat = ['-', '-', '-', '-', stan_dev(lakenw_meas[4], lakenw_means[4]),
                     stan_dev(lakenw_meas[5], lakenw_means[5]), stan_dev(lakenw_meas[6], lakenw_means[6]),
                     stan_dev(lakenw_meas[7], lakenw_means[7]), stan_dev(lakenw_meas[8], lakenw_means[8]), '-',
                     stan_dev(lakenw_meas[10], lakenw_means[10]), stan_dev(lakenw_meas[11], lakenw_means[11]),
                     stan_dev(lakenw_meas[12], lakenw_means[12]), stan_dev(lakenw_meas[13], lakenw_means[13])]
  lakese_stan_deviat = ['-', '-', '-', '-', stan_dev(lakese_meas[4], lakese_means[4]),
                     stan_dev(lakese_meas[5], lakese_means[5]), stan_dev(lakese_meas[6], lakese_means[6]),
                     stan_dev(lakese_meas[7], lakese_means[7]), stan_dev(lakese_meas[8], lakese_means[8]), '-',
                     stan_dev(lakese_meas[10], lakese_means[10]), stan_dev(lakese_meas[11], lakese_means[11]),
                     stan_dev(lakese_meas[12], lakese_means[12]), stan_dev(lakese_meas[13], lakese_means[13])]
  lakesw_stan_deviat = ['-', '-', '-', '-', stan_dev(lakesw_meas[4], lakesw_means[4]),
                     stan_dev(lakesw_meas[5], lakesw_means[5]), stan_dev(lakesw_meas[6], lakesw_means[6]),
                     stan_dev(lakesw_meas[7], lakesw_means[7]), stan_dev(lakesw_meas[8], lakesw_means[8]), '-',
                     stan_dev(lakesw_meas[10], lakesw_means[10]), stan_dev(lakesw_meas[11], lakesw_means[11]),
                     stan_dev(lakesw_meas[12], lakesw_means[12]), stan_dev(lakesw_meas[13], lakesw_means[13])]
   # calculate skewness
  total_skew = ['-', '-', '-', '-', skew_calc(total_meas[4], total_means[4]),
                skew_calc(total_meas[5], total_means[5]), skew_calc(total_meas[6], total_means[6]),
                skew_calc(total_meas[7], total_means[7]), skew_calc(total_meas[8], total_means[8]), '-',
                skew_calc(total_meas[10], total_means[10]), skew_calc(total_meas[11], total_means[11]),
                skew_calc(total_meas[12], total_means[12]), skew_calc(total_meas[13], total_means[13])]

  laken_skew = ['-', '-', '-', '-', skew_calc(laken_meas[4], laken_means[4]),
                skew_calc(laken_meas[5], laken_means[5]), skew_calc(laken_meas[6], laken_means[6]),
                skew_calc(laken_meas[7], laken_means[7]), skew_calc(laken_meas[8], laken_means[8]), '-',
                skew_calc(laken_meas[10], laken_means[10]), skew_calc(laken_meas[11], laken_means[11]),
                skew_calc(laken_meas[12], laken_means[12]), skew_calc(laken_meas[13], laken_means[13])]
  lakenw_skew = ['-', '-', '-', '-', skew_calc(lakenw_meas[4], lakenw_means[4]),
                skew_calc(lakenw_meas[5], lakenw_means[5]), skew_calc(lakenw_meas[6], lakenw_means[6]),
                skew_calc(lakenw_meas[7], lakenw_means[7]), skew_calc(lakenw_meas[8], lakenw_means[8]), '-',
                skew_calc(lakenw_meas[10], lakenw_means[10]), skew_calc(lakenw_meas[11], lakenw_means[11]),
                skew_calc(lakenw_meas[12], lakenw_means[12]), skew_calc(lakenw_meas[13], lakenw_means[13])]
  lakese_skew = ['-', '-', '-', '-', skew_calc(lakese_meas[4], lakese_means[4]),
                skew_calc(lakese_meas[5], lakese_means[5]), skew_calc(lakese_meas[6], lakese_means[6]),
                skew_calc(lakese_meas[7], lakese_means[7]), skew_calc(lakese_meas[8], lakese_means[8]), '-',
                skew_calc(lakese_meas[10], lakese_means[10]), skew_calc(lakese_meas[11], lakese_means[11]),
                skew_calc(lakese_meas[12], lakese_means[12]), skew_calc(lakese_meas[13], lakese_means[13])]
  lakesw_skew = ['-', '-', '-', '-', skew_calc(lakesw_meas[4], lakesw_means[4]),
                skew_calc(lakesw_meas[5], lakesw_means[5]), skew_calc(lakesw_meas[6], lakesw_means[6]),
                skew_calc(lakesw_meas[7], lakesw_means[7]), skew_calc(lakesw_meas[8], lakesw_means[8]), '-',
                skew_calc(lakesw_meas[10], lakesw_means[10]), skew_calc(lakesw_meas[11], lakesw_means[11]),
                skew_calc(lakesw_meas[12], lakesw_means[12]), skew_calc(lakesw_meas[13], lakesw_means[13])]
  # calculate kurtosis
  total_kurt = ['-', '-', '-', '-', kurtosis_calc(total_meas[4], total_means[4]),
                kurtosis_calc(total_meas[5], total_means[5]), kurtosis_calc(total_meas[6], total_means[6]),
                kurtosis_calc(total_meas[7], total_means[7]), kurtosis_calc(total_meas[8], total_means[8]), '-',
                kurtosis_calc(total_meas[10], total_means[10]), kurtosis_calc(total_meas[11], total_means[11]),
                kurtosis_calc(total_meas[12], total_means[12]), kurtosis_calc(total_meas[13], total_means[13])]
     
  laken_kurt = ['-', '-', '-', '-', kurtosis_calc(laken_meas[4], laken_means[4]),
                kurtosis_calc(laken_meas[5], laken_means[5]), kurtosis_calc(laken_meas[6], laken_means[6]),
                kurtosis_calc(laken_meas[7], laken_means[7]), kurtosis_calc(laken_meas[8], laken_means[8]), '-',
                kurtosis_calc(laken_meas[10], laken_means[10]), kurtosis_calc(laken_meas[11], laken_means[11]),
                kurtosis_calc(laken_meas[12], laken_means[12]), kurtosis_calc(laken_meas[13], laken_means[13])]
  lakenw_kurt = ['-', '-', '-', '-', kurtosis_calc(lakenw_meas[4], lakenw_means[4]),
                kurtosis_calc(lakenw_meas[5], lakenw_means[5]), kurtosis_calc(lakenw_meas[6], lakenw_means[6]),
                kurtosis_calc(lakenw_meas[7], lakenw_means[7]), kurtosis_calc(lakenw_meas[8], lakenw_means[8]), '-',
                kurtosis_calc(lakenw_meas[10], lakenw_means[10]), kurtosis_calc(lakenw_meas[11], lakenw_means[11]),
                kurtosis_calc(lakenw_meas[12], lakenw_means[12]), kurtosis_calc(lakenw_meas[13], lakenw_means[13])]
  lakese_kurt = ['-', '-', '-', '-', kurtosis_calc(lakese_meas[4], lakese_means[4]),
                kurtosis_calc(lakese_meas[5], lakese_means[5]), kurtosis_calc(lakese_meas[6], lakese_means[6]),
                kurtosis_calc(lakese_meas[7], lakese_means[7]), kurtosis_calc(lakese_meas[8], lakese_means[8]), '-',
                kurtosis_calc(lakese_meas[10], lakese_means[10]), kurtosis_calc(lakese_meas[11], lakese_means[11]),
                kurtosis_calc(lakese_meas[12], lakese_means[12]), kurtosis_calc(lakese_meas[13], lakese_means[13])]
  lakesw_kurt = ['-', '-', '-', '-', kurtosis_calc(lakesw_meas[4], lakesw_means[4]),
                kurtosis_calc(lakesw_meas[5], lakesw_means[5]), kurtosis_calc(lakesw_meas[6], lakesw_means[6]),
                kurtosis_calc(lakesw_meas[7], lakesw_means[7]), kurtosis_calc(lakesw_meas[8], lakesw_means[8]), '-',
                kurtosis_calc(lakesw_meas[10], lakesw_means[10]), kurtosis_calc(lakesw_meas[11], lakesw_means[11]),
                kurtosis_calc(lakesw_meas[12], lakesw_means[12]), kurtosis_calc(lakesw_meas[13], lakesw_means[13])]

  # specify vectors containing column headers and flower types
  # create and print out dataframe for total data set
  data_type = ['Continuous', 'Continuous', 'Continuous', 'Continuous', 'Continuous', 'Continuous', 'Nominal',
               'Continuous', 'Continuous', 'Continuous', 'Continuous']
  description = ['Date and Time of Reading', 'Outside Temperature', 'Temperature that dew would form',
                'Current Wind Speed', 'Maximum Wind Speed in last 10 minutes', 'Direction of wind with North being 0',
                'Wind General Direction', 'Amount of Rain in last hour', 'Humidity', 'Solar Radiation',
                'Absolute Air Pressure']
  total_data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : total_mins[3::],
          'Maximum' : total_maxs[3::], 'Mean' : total_means[3::], 'Trimmed Means' : total_trim_means[3::], 'Mode' : total_modes[3::],
          'Median' : total_median[3::], 'Standard Deviation' : total_stan_deviat[3::], 'Skewness' : total_skew[3::],
          'Kurtosis' : total_kurt[3::]}
  total_df = pd.DataFrame(total_data).set_index('Variable')

  laken_data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : laken_mins[3::],
          'Maximum' : laken_maxs[3::], 'Mean' : laken_means[3::], 'Trimmed Means' : laken_trim_means[3::], 'Mode' : laken_modes[3::],
          'Median' : laken_median[3::], 'Standard Deviation' : laken_stan_deviat[3::], 'Skewness' : laken_skew[3::],
          'Kurtosis' : laken_kurt[3::]}
  laken_df = pd.DataFrame(laken_data).set_index('Variable')
  
  lakenw_data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : lakenw_mins[3::],
          'Maximum' : lakenw_maxs[3::], 'Mean' : lakenw_means[3::], 'Trimmed Means' : lakenw_trim_means[3::], 'Mode' : lakenw_modes[3::],
          'Median' : lakenw_median[3::], 'Standard Deviation' : lakenw_stan_deviat[3::], 'Skewness' : lakenw_skew[3::],
          'Kurtosis' : lakenw_kurt[3::]}
  lakenw_df = pd.DataFrame(lakenw_data).set_index('Variable')
  
  lakese_data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : lakese_mins[3::],
          'Maximum' : lakese_maxs[3::], 'Mean' : lakese_means[3::], 'Trimmed Means' : lakese_trim_means[3::], 'Mode' : lakese_modes[3::],
          'Median' : lakese_median[3::], 'Standard Deviation' : lakese_stan_deviat[3::], 'Skewness' : lakese_skew[3::],
          'Kurtosis' : lakese_kurt[3::]}
  lakese_df = pd.DataFrame(lakese_data).set_index('Variable')
  
  lakesw_data = {'Variable' : variables, 'Data Type' : data_type, 'Description' : description, 'Minimum' : lakesw_mins[3::],
          'Maximum' : lakesw_maxs[3::], 'Mean' : lakesw_means[3::], 'Trimmed Means' : lakesw_trim_means[3::], 'Mode' : lakesw_modes[3::],
          'Median' : lakesw_median[3::], 'Standard Deviation' : lakesw_stan_deviat[3::], 'Skewness' : lakesw_skew[3::],
          'Kurtosis' : lakesw_kurt[3::]}
  lakesw_df = pd.DataFrame(lakesw_data).set_index('Variable')

  # create and print out dataframe for class data sets
  total_df.to_csv('stats.csv', index=True, mode='w')
  laken_df.to_csv('stats.csv', index=True, mode='a')
  lakenw_df.to_csv('stats.csv', index=True, mode='a')
  lakese_df.to_csv('stats.csv', index=True, mode='a')
  lakesw_df.to_csv('stats.csv', index=True, mode='a')